import { Component, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage {
  @ViewChild(IonTabs) tabs: any;
  counter = 0;

  constructor() { }

  ionViewWillEnter() {
    if (this.counter > 1) {
      this.tabs?.outlet?.activated?.instance?.ionViewWillEnter?.();
    }
    this.counter++;
  }

  onTabChange() {
    this.counter++;
  }
}
