import { Component, OnInit, OnChanges } from '@angular/core';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-custom-alert',
  templateUrl: './custom-alert.component.html',
  styleUrls: ['./custom-alert.component.scss'],
})
export class CustomAlertComponent implements OnInit, OnChanges {
  options: CustomAlertOptions;
  primaryButton: any;
  secondaryButton: any;

  constructor(private navParams: NavParams) {
    this.options = this.navParams.get('options') as CustomAlertOptions;
    [this.primaryButton, this.secondaryButton] = this.options?.buttons || [];
  }

  ngOnInit() {
    if (this.options) {
      this.options = {
        ...this.options,
        img: this.getImg(),
        color: this.getColor()
      };
    }
  }

  ngOnChanges() {
    if (this.options) {
      this.options = {
        ...this.options,
        img: this.getImg(),
        color: this.getColor()
      };
    }
  }

  private getImg() {
    if (this.options?.img) {
      return this.options.img;
    }

    if (this.options?.type === 'success') {
      return 'assets/img/success.svg';
    }

    if (this.options?.type === 'warning') {
      return 'assets/img/warning.svg';
    }

    if (this.options?.type === 'error') {
      return 'assets/img/error.svg';
    }

    if (this.options?.type === 'info') {
      return 'assets/img/info.svg';
    }

    return undefined;
  }

  private getColor() {
    if (this.options?.color) {
      return this.options.color;
    }

    if (this.options.type === 'success') {
      return 'success';
    }

    if (this.options.type === 'warning') {
      return 'warning';
    }

    if (this.options.type === 'info') {
      return 'primary';
    }

    if (this.options.type === 'error') {
      return 'danger';
    }

    return 'primary';
  }
}

export interface CustomAlertOptions {
  type?: 'success' | 'warning' | 'error' | 'info';
  img?: string;
  main?: string;
  spinner?: boolean;
  header?: string;
  message?: string;
  color?: 'primary' | 'secondary' | 'tertiary' | 'success' | 'warning' | 'danger' | 'light' | 'medium' | 'dark';
  buttons?: CustomAlertButtons[];
}

export type CustomAlertButtons = {
  text: string,
  handler: any | void | Promise<any> | Promise<void>
};
