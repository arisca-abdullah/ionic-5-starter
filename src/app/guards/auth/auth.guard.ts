import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { UtilsService } from 'src/app/services/utils/utils.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  noAuthPages = [];

  constructor(private utils: UtilsService) {}

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
      // const atNeedAuthPage = !this.noAuthPages.includes(state.url);
      // const isAuthenticated = await this.utils.isAuthenticated();

      // if (!atNeedAuthPage && !isAuthenticated || atNeedAuthPage && isAuthenticated) {
        return true;
      // }

      // this.utils.goRoot(!atNeedAuthPage ? '/tabs/home' : this.noAuthPages[0]);
      // return false;
  }

}
