import { Injectable } from '@angular/core';
import { AlertButton } from '@ionic/core';
import { HTTP, HTTPResponse } from '@ionic-native/http/ngx';
import { UtilsService } from 'src/app/services/utils/utils.service';
import { CustomAlertOptions } from 'src/app/components/custom-alert/custom-alert.component';

@Injectable({
  providedIn: 'root'
})
export class WebApiService {

  constructor(private http: HTTP, private utils: UtilsService) { }

  async request(requestSet: RequestSet, options: RequestSetOptions) {
    await this.requests([requestSet], options);
  }

  async requests(requestSets: RequestSet[], options: RequestSetOptions) {
    try {
      if (requestSets.length) {
        const responses = await Promise.all(requestSets.map(request => request()));
        const responsesBody = responses.map(response => JSON.parse(response.data));
        await options.onSuccess?.(responsesBody);
      }
    } catch (error) {
      options?.onError ? await options.onError(error) : await this.handleError(error);
    } finally {
      await options?.onComplete?.();
    }
  }

  async get(url: string, params: any, headers: any = {}) {
    await this.utils.platformReady();
    return this.http.get(url, params, headers);
  }

  async post(url: string, body: any, headers: any = {}) {
    await this.utils.platformReady();
    return this.http.post(url, body, headers);
  }

  async put(url: string, body: any, headers: any = {}) {
    await this.utils.platformReady();
    return this.http.put(url, body, headers);
  }

  async patch(url: string, body: any, headers: any = {}) {
    await this.utils.platformReady();
    return this.http.patch(url, body, headers);
  }

  async delete(url: string, params: any, headers: any = {}) {
    await this.utils.platformReady();
    return this.http.delete(url, params, headers);
  }

  handleError(error: any, extra: OptionalAlertOpions = {}) {
    console.error(error);
    return 'status' in error ? this.handleHTTPError(error, extra) : this.handleErrorMessage(error, extra);
  }

  handleHTTPError(error: HTTPResponse, extra: OptionalAlertOpions = {}) {
    return error.status < 0 ? this.handleSimpleHTTPError(error, extra) : this.handleErrorBody(error, extra);
  }

  handleSimpleHTTPError(error: HTTPResponse, extra: OptionalAlertOpions = {}) {
    const { disableBackdropDismiss, ...extraOptions } = {
      type: 'error',
      header: 'Error',
      message: error.error,
      buttons: [{
        text: 'Okay',
        handler: () => this.utils.dismissPopover()
      }],
      ...extra
    };

    return this.utils.createCustomAlert(extraOptions as CustomAlertOptions, disableBackdropDismiss);
  }

  handleErrorBody(error: HTTPResponse, extra: OptionalAlertOpions = {}) {
    try {
      const errorBody = JSON.parse(error.error);
      return 'message' in errorBody ? this.handleErrorMessage(errorBody, extra) : this.handleSimpleHTTPError(error, extra);
    } catch (err) {
      return 'error' in error ? this.handleSimpleHTTPError(error, extra) : this.handleErrorMessage(err, extra);
    }
  }

  handleErrorMessage(error: any, extra: OptionalAlertOpions = {}) {
    const { disableBackdropDismiss, ...extraOptions } = {
      type: 'error',
      header: 'Error',
      message: error.message,
      buttons: [{
        text: 'Okay',
        handler: () => this.utils.dismissPopover()
      }],
      ...extra
    };

    return this.utils.createCustomAlert(extraOptions as CustomAlertOptions, disableBackdropDismiss);
  }
}

interface OptionalAlertOpions extends CustomAlertOptions {
  disableBackdropDismiss?: boolean;
}

export type RequestSet = () => Promise<HTTPResponse>;
export type RequestSetOptions = {
  onSuccess?: (responses: any[]) => void | any | Promise<void> | Promise<any>,
  onError?: (error: any) => void | any | Promise<void> | Promise<any>,
  onComplete?: () => void | any | Promise<void> | Promise<any>
};
