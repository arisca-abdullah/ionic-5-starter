import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import {
  ActionSheetOptions,
  AlertOptions,
  LoadingOptions,
  PopoverOptions,
  ToastOptions
} from '@ionic/core';

import {
  Platform,
  NavController,
  ActionSheetController,
  AlertController,
  LoadingController,
  PopoverController,
  ToastController
} from '@ionic/angular';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { BuildInfo } from '@ionic-native/build-info/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import * as moment from 'moment';
import { CustomAlertComponent, CustomAlertOptions } from 'src/app/components/custom-alert/custom-alert.component';
import { environment as env } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  private temporaryData: any;

  constructor(
    private router: Router,
    private platform: Platform,
    private navCtrl: NavController,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private popoverCtrl: PopoverController,
    private toastCtrl: ToastController,
    private androidPermissions: AndroidPermissions,
    private buildInfo: BuildInfo,
    private screenOrientation: ScreenOrientation,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private nativeStorage: NativeStorage) { }

  getTemporaryData() {
    return this.temporaryData;
  }

  setTemporaryData(data: any) {
    this.temporaryData = data;
  }

  validateEmail(email: string) {
    return env.patterns.email.test(email);
  }

  parseParamMap(paramMap: string) {
    paramMap = paramMap.split('%2F').join('/');

    try {
      return JSON.parse(paramMap);
    } catch (error) {
      return paramMap;
    }
  }

  formatDate(format: string, input: moment.MomentInput = moment()) {
    return moment(input).format(format);
  }

  platformReady() {
    return this.platform.ready();
  }

  overrideBackButton(callback: () => any) {
    return this.platform.backButton.subscribeWithPriority(10, callback);
  }

  nav(route: string, params?: any) {
    if (params) {
      params = typeof params === 'string' ? params : JSON.stringify(params);
      params = params.split('/').join('%2F');
      route += `/${params}`;
    }

    return this.router.navigate([route]);
  }

  navByUrl(route: string) {
    return this.router.navigateByUrl(route);
  }

  goRoot(route: string) {
    return this.navCtrl.navigateRoot(route);
  }

  back() {
    return this.navCtrl.pop();
  }

  exitApp() {
    // tslint:disable-next-line: no-string-literal
    navigator['app'].exitApp();
  }

  async createActionSheet(options: ActionSheetOptions) {
    const actionSheet = await this.actionSheetCtrl.create(options);
    actionSheet.present();

    return actionSheet;
  }

  async createAlert(options: AlertOptions) {
    const alert = await this.alertCtrl.create(options);
    alert.present();

    return alert;
  }

  async createLoader(options: LoadingOptions) {
    const loader = await this.loadingCtrl.create(options);
    loader.present();

    return loader;
  }

  async createPopover(options: PopoverOptions) {
    const popover = await this.popoverCtrl.create(options);
    popover.present();

    return popover;
  }

  dismissPopover(data?: any) {
    return this.popoverCtrl.dismiss(data);
  }

  async createToast(options: ToastOptions) {
    const toast = await this.toastCtrl.create(options);
    toast.present();

    return toast;
  }

  createCustomAlert(options: CustomAlertOptions, disableBackdropDismiss = false) {
    return this.createPopover({
      component: CustomAlertComponent,
      componentProps: { options },
      cssClass: 'alert-popover',
      mode: 'ios',
      backdropDismiss: !disableBackdropDismiss
    });
  }

  async checkPermission(permission: string) {
    await this.platformReady();
    return this.androidPermissions.checkPermission(permission);
  }

  async requestPermission(permission: string) {
    await this.platformReady();
    return this.androidPermissions.requestPermission(permission);
  }

  async requestPermissions(permissions: string[]) {
    await this.platformReady();
    return this.androidPermissions.requestPermissions(permissions);
  }

  async getBasePackageName() {
    await this.platformReady();
    return this.buildInfo.basePackageName;
  }

  async getBaseUrl() {
    await this.platformReady();
    return this.buildInfo.baseUrl;
  }

  async getBuildDate() {
    await this.platformReady();
    return this.buildInfo.buildDate;
  }

  async getBuildType() {
    await this.platformReady();
    return this.buildInfo.buildType;
  }

  async getDebug() {
    await this.platformReady();
    return this.buildInfo.debug;
  }

  async getDisplayName() {
    await this.platformReady();
    return this.buildInfo.displayName;
  }

  async getInstallDate() {
    await this.platformReady();
    return this.buildInfo.installDate;
  }

  async getName() {
    await this.platformReady();
    return this.buildInfo.name;
  }

  async getPackageName() {
    await this.platformReady();
    return this.buildInfo.packageName;
  }

  async getVersion() {
    await this.platformReady();
    return this.buildInfo.version;
  }

  async getVersionCode() {
    await this.platformReady();
    return this.buildInfo.versionCode;
  }

  async lockOrientation(orientation: string) {
    await this.platformReady();
    return this.screenOrientation.lock(orientation);
  }

  async unlockOrientation() {
    await this.platformReady();
    return this.screenOrientation.unlock();
  }

  async currentOrientation() {
    await this.platformReady();
    return this.screenOrientation.type;
  }

  async onOrientationChange() {
    await this.platformReady();
    return this.screenOrientation.onChange();
  }

  async setStatusBarLightContent() {
    await this.platformReady();
    return this.statusBar.styleLightContent();
  }

  async setStatusBarOverlaysWebView(doesOverlay: boolean) {
    await this.platformReady();
    return this.statusBar.overlaysWebView(doesOverlay);
  }

  async setStatusBarColor(hexString: string) {
    await this.platformReady();
    return this.statusBar.backgroundColorByHexString(hexString);
  }

  async hideSplashScreen() {
    await this.platformReady();
    return this.splashScreen.hide();
  }

  async getStorage(key: string) {
    await this.platformReady();
    return this.nativeStorage.getItem(key);
  }

  async setStorage(key: string, value: any) {
    await this.platformReady();
    return this.nativeStorage.setItem(key, value);
  }

  async removeStorage(key: string) {
    await this.platformReady();
    return this.nativeStorage.remove(key);
  }

  async clearStorage() {
    await this.platformReady();
    return this.nativeStorage.clear();
  }

  async isAuthenticated() {
    try {
      return await this.getStorage('isAuthenticated') as boolean;
    } catch (error) {
      return await this.setStorage('isAuthenticated', false) as boolean;
    }
  }

}

export { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
export { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
export { environment as env } from 'src/environments/environment';
