import { Component } from '@angular/core';
import {
  UtilsService,
  AndroidPermissions,
  ScreenOrientation
} from 'src/app/services/utils/utils.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  permissions: any[] = [
    { name: this.androidPermissions.PERMISSION.INTERNET },
  ];

  constructor(
    private androidPermissions: AndroidPermissions,
    private screenOrientation: ScreenOrientation,
    private utils: UtilsService) {
      this.initializeApp();
  }

  initializeApp() {
    this.utils.setStatusBarLightContent();
    this.utils.hideSplashScreen();
    this.utils.lockOrientation(this.screenOrientation.ORIENTATIONS.PORTRAIT_PRIMARY);
    setTimeout(() => {
      this.utils.createCustomAlert({
        type: 'info',
        header: 'Hello, World!',
        buttons: [{ text: 'Close', handler: () => this.utils.dismissPopover() }]
      });
    }, 3000);
  }

  async checkAppPermissions() {
    for (const permission of this.permissions) {
      const response = await this.utils.checkPermission(permission.name);
      permission.hasPermission = response.hasPermission;
    }

    const needRequest = this.permissions.filter(permission => !permission.hasPermission);

    if (needRequest.length) {
      this.utils.requestPermissions(needRequest.map(permission => permission.name));
    }
  }

}
